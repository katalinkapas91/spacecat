package model;

public class Alien {

    private int x;
    private int y;
    private float speedY;


    public Alien(int y, float speedY) {
        this.x = (int) (Math.random() * 800);
        this.y = y;
        this.speedY = speedY;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getSpeedY() {
        return speedY;
    }

    public void update(long timePassed) {
        this.y = Math.round(this.y + timePassed * this.speedY);
    }

    public int getHeight() {
        return 50;
    }

    public int getWidth() {
        return 50;
    }


}
