package model;


public class Player {

    private Missile missile;


    //Position
    private int x;
    private int y;
    private int currentLife = 10;
    private int score;

    private String name;

    public String getName() {
        return name;
    }

    private boolean isCollision = false; //if collision -> true

    public boolean isCollision() {
        return isCollision;
    }

    public void setCollision(boolean collision) {
        isCollision = collision;
    }

    public Player() {

    }

    public Player(String name) {
        this.name = name;
    }

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.missile = missile;
    }

    public void move(int dx, int dy) {

        if (this.x < 25) {
            this.x = 25;
        } else if (this.x > Model.WIDTH - 100) {
            this.x = 700;
        } else {
            this.x += 4 * dx;
        }

        if (this.y < 25) {
            this.y = 25;
        } else if (this.y > Model.WIDTH - 100) {
            this.y = 700;
        } else {
            this.y += 4 * dy;
        }


    }


    public void update(long timePassed) {

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return 60;
    }

    public int getWidth() {
        return 60;
    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }


    public void decreaseCurrentLife() {
        if (isCollision) {
            currentLife -= 1;
        }
    }

    public int getCurrentLife() {
        return currentLife;
    }

    public int getScore() {
        return score;
    }

    public void increaseScore() {
        score += 5;
    }
}
