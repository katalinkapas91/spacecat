package model;

import java.util.LinkedList;
import java.util.List;

public class Model {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 800;

    private Player player;
    private List<Alien> aliens = new LinkedList<>();
    private Missile missile;

    private boolean isGameOver;
    private boolean fired = false;
    private boolean shot = false;


    public boolean isFired() {
        return fired;
    }

    public void setFired(boolean fired) {
        this.fired = fired;
    }

    public Model() {
        this.player = new Player(375, 740);
        this.aliens.add(new Alien(50, 0.2f));
        this.missile = new Missile(player.getX(), player.getY());
    }

    public Player getPlayer() {
        return player;
    }

    public List<Alien> getAlien() {
        return aliens;
    }

    public Missile getMissile() {

        return missile;
    }

    public boolean isGameOver() {
        return isGameOver;
    }

    public void shoot() {
        missile = new Missile(player.getX() + player.getWidth() / 2, player.getY() - player.getHeight() / 2);
    }


    public void update(long timePassed) {


        if (!isGameOver) {
            player.update(timePassed);

            for (Alien alien : aliens) {
                alien.update(timePassed);
            }

            aliens.removeIf(alien -> alien.getY() > 686);

            if (Math.random() * timePassed > 20) {
                this.aliens.add(new Alien(50, 0.1f));
            }

            missile.update(timePassed);
            if (isFired()) {
                shoot();
            }
            missile.update(timePassed);


            for (Alien alien : aliens) {
                int dx = Math.abs(missile.getX() - alien.getX());
                int dy = Math.abs(missile.getY() - alien.getY());
                int dw = missile.getWidth() / 2 + alien.getWidth() / 2;
                int dh = missile.getHeight() / 2 + alien.getHeight() / 2;

                if (dx <= dw && dy <= dh) {
                    aliens.remove(alien);
                    player.increaseScore();
                    System.out.println(player.getScore());
                    shot = false;
                }
            }


        }

        for (Alien alien : aliens) {
            int dx = Math.abs(player.getX() - alien.getX());
            int dy = Math.abs(player.getY() - alien.getY());
            int dw = player.getWidth() / 2 + alien.getWidth() / 2;
            int dh = player.getHeight() / 2 + alien.getHeight() / 2;


            if (dx <= dw && dy <= dh) {

                player.setCollision(true);
                player.decreaseCurrentLife();
                System.out.println(player.getCurrentLife());
                player.moveTo(375, 740);
            }

            if (player.getCurrentLife() == 0) {
                isGameOver = true;
            }

        }
    }


}


