package model;

public class Missile {

    private int x;
    private int y;
    private float speedY;


    public Missile(int x, int y) {
        this.x = x;
        this.y = y;
        this.speedY = 0.2f;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getSpeedY() {
        return speedY;
    }

    public void update(long timePassed) {
        this.y = Math.round(this.y + timePassed * this.speedY * -1);
    }


    public int getHeight() {
        return 10;
    }

    public int getWidth() {
        return 5;
    }


}
