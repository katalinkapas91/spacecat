import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Model;
import model.Player;


public class Main extends Application {


    private Timer timer;

    @Override
    public void start(Stage primaryStage) throws Exception {

        Stage newStage = new Stage();
        newStage.setTitle("SpaceCat");
        FormLayout formLayout = new FormLayout();
        newStage.initModality(Modality.APPLICATION_MODAL);
        newStage.setScene(new Scene(formLayout, 300, 200));

        newStage.show();

        formLayout.setResultHandler(new FormLayout.ResultHandler() {
            @Override
            public void onSubmit(Player result) {
                Stage greetingStage = new Stage();

                Greeting greetingLayout = new Greeting(result);
                greetingStage.initOwner(newStage);
                greetingStage.initModality(Modality.WINDOW_MODAL);
                greetingStage.setScene(new Scene(greetingLayout, 300, 200));
                greetingStage.showAndWait();
                greetingStage.close();
                newStage.close();


                Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);
                Group group = new Group();
                group.getChildren().add(canvas);
                Scene scene = new Scene(group);
                ImagePattern pattern = new ImagePattern(new Image("galaxy.jpg"));
                scene.setFill(pattern);

                primaryStage.setScene(scene);

                GraphicsContext gc = canvas.getGraphicsContext2D();

                Model model = new Model();
                Player player = new Player();
                Graphics graphics = new Graphics(model, gc, player);


                InputHandler inputHandler = new InputHandler(model);
                scene.setOnKeyPressed(event -> inputHandler.onKeyPressed(event.getCode()));
                scene.setOnKeyReleased(event -> inputHandler.onKeyReleased(event.getCode()));


                timer = new Timer(model, graphics);
                timer.start();

                primaryStage.show();


            }
        });


    }



    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }


}
