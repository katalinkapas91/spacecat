import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Alien;
import model.Missile;
import model.Model;
import model.Player;

public class Graphics {

    Image iconCat = new Image("SpaceCat.png");
    Image iconAlien = new Image("alien.jpg");
    Image gameOver = new Image("over.jpg");


    private Model model;
    private GraphicsContext gc;
    private Player player;
    private Missile missile;


    public Graphics(Model model, GraphicsContext gc, Player player) {
        this.model = model;
        this.gc = gc;
        this.player = player;
    }


    public void draw() {

        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

        gc.drawImage(
                iconCat,
                model.getPlayer().getX(),
                model.getPlayer().getY(),
                player.getWidth(),
                player.getHeight()
        );


        for (Alien alien : this.model.getAlien()) {
            gc.drawImage(
                    iconAlien,
                    alien.getX(),
                    alien.getY(),
                    alien.getWidth(),
                    alien.getHeight()

            );

        }


        gc.setFill(Color.RED);
        gc.fillRect(
                model.getMissile().getX(),
                model.getMissile().getY(),
                5,
                10
        );


        int life = model.getPlayer().getCurrentLife();
        String text = String.valueOf(life);
        int score = model.getPlayer().getScore();
        String score2 = String.valueOf(score);


        gc.setFill(Color.RED);
        gc.setFont(Font.font("Verdana", 30));
        gc.fillText("Life: " + text + "\nScore: " + score2, 20, 50);

        if (model.isGameOver()) {
            gc.drawImage(
                    gameOver,
                    200,
                    200,
                    500,
                    500

            );

        }


    }


}




