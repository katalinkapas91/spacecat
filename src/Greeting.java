import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import model.Player;

public class Greeting extends VBox {


    public Greeting(Player player) {
        super();
        setSpacing(10);
        setPadding(new Insets(50, 50, 50, 60));
        setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        Label text = new Label("Welcome " + player.getName() + "! \n Let`s play!");
        text.setTextFill(Color.WHITE);
        text.setFont(new Font("Verdana", 20));
        text.setTextAlignment(TextAlignment.CENTER);

        text.setWrapText(true);

        Button yesButton = new Button("GO");
        yesButton.setDefaultButton(true);
        yesButton.setLayoutX(500);
        yesButton.setLayoutY(500);


        yesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                getScene().getWindow().hide();
            }
        });


        ButtonBar buttonbar = new ButtonBar();
        buttonbar.setLayoutX(100);
        buttonbar.setLayoutY(100);
        buttonbar.getButtons().addAll(yesButton);

        getChildren().addAll(text, buttonbar);

    }


}
