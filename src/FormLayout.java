import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.Player;


public class FormLayout extends VBox {

    public interface ResultHandler {
        void onSubmit(Player result);
    }

    private ResultHandler resultHandler = null;


    private TextField nameField = new TextField();


    public FormLayout() {
        super();
        setSpacing(10);
        setPadding(new Insets(10, 10, 10, 10));
        setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));

        Button submitButton = new Button("ok");
        submitButton.setAlignment(Pos.CENTER);
        submitButton.setDefaultButton(true);
        submitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (resultHandler != null) {
                    Player player = new Player(nameField.getText());
                    resultHandler.onSubmit(player);
                }

            }

        });

        ButtonBar buttonBar = new ButtonBar();
        buttonBar.getButtons().addAll(submitButton);

        Label label = new Label("Please enter your name");
        label.setTextFill(Color.WHITE);
        label.setAlignment(Pos.CENTER);
        label.setFont(new Font("Verdana", 20));
        this.getChildren().addAll(

                label,
                nameField,
                buttonBar

        );

    }

    public void setResultHandler(ResultHandler resultHandler) {
        this.resultHandler = resultHandler;
    }

}
